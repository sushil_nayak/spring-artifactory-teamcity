package com.nayak.springartifactoryteamcity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringArtifactoryTeamcityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringArtifactoryTeamcityApplication.class, args);
	}
}
